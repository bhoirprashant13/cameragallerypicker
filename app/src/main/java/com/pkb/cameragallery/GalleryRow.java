package com.pkb.cameragallery;

/**
 * Created by Ravi on 29/07/15.
 */
public class GalleryRow {
    private String title;
    private String img_uri;

    public GalleryRow() {
    }

    public String getImg_uri() {
        return img_uri;
    }

    public void setImg_uri(String img_uri) {
        this.img_uri = img_uri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
