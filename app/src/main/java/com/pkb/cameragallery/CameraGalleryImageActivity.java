package com.pkb.cameragallery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class CameraGalleryImageActivity extends AppCompatActivity implements SurfaceHolder.Callback{

    RecyclerView mRecyclerView;
    ArrayList<GalleryRow> contactRows=new ArrayList<GalleryRow>();
    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;

    Camera.PictureCallback rawCallback;
    Camera.ShutterCallback shutterCallback;
    Camera.PictureCallback jpegCallback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mRecyclerView = (RecyclerView)findViewById(R.id.drawerList);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mRecyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),3));


        Uri uri;
        Cursor cursor;
        int column_index_data;
        String PathOfImage = null;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

        cursor = getApplicationContext().getContentResolver().query(uri, projection, null, null, null);
        if(cursor.getCount() > 0) {
            column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            while (cursor.moveToNext()) {
                PathOfImage = cursor.getString(column_index_data);
                GalleryRow contactRow=new GalleryRow();
                contactRow.setImg_uri(PathOfImage);
                contactRows.add(contactRow);
            }
        }
        cursor.close();


        mRecyclerView.setAdapter(new GalleryAdapter(contactRows,this));






        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        surfaceHolder.addCallback(this);

        // deprecated setting, but required on Android versions prior to 3.0
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        jpegCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {

                if (data != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data , 0, data .length);

                    if(bitmap!=null){

                        File file=new File(Environment.getExternalStorageDirectory()+"/Prashant");
                        if(!file.isDirectory()){
                            file.mkdir();
                        }

                        file=new File(Environment.getExternalStorageDirectory()+"/Prashant",System.currentTimeMillis()+".jpg");


                        try
                        {
                            //file.createNewFile();
                            FileOutputStream fileOutputStream=new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG,100, fileOutputStream);

                            fileOutputStream.flush();
                            fileOutputStream.close();

                            Toast.makeText(getApplicationContext(), "Picture Saved", Toast.LENGTH_SHORT).show();
                            refreshCamera();

                            Intent intent=new Intent();
                            intent.putExtra("path",file.getAbsolutePath().toString());
                            setResult(2,intent);
                            finish();//finishing activity
                        }
                        catch(IOException e){
                            e.printStackTrace();
                        }
                        catch(Exception exception)
                        {
                            exception.printStackTrace();
                        }

                    }
                }


//                FileOutputStream outStream = null;
//                try {
//                    String root = Environment.getExternalStorageDirectory().toString();
//                    File myDir = new File(root + "/Prashant");
//                    if (!myDir.exists()){
//                        myDir.mkdir();
//                    }
//
//
//                    outStream = new FileOutputStream(String.format("/"+myDir.getAbsolutePath()+"/%d.jpg", System.currentTimeMillis()));
//                    outStream.write(data);
//                    outStream.close();
//
//                    File outpt = new File( sdCardDirectory, "photo.jpeg" );
//                    FileOutputStream outptOs = new FileOutputStream( outpt );
//                    photo.compress( Bitmap.CompressFormat.JPEG, 90, outptOs );
//                    Log.d("Log", "onPictureTaken - wrote bytes: " + data.length);
//
//
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } finally {
//                }

            }
        };

    }
    public void setCameraDisplayOrientation()
    {
        if (camera == null)
        {
            Log.d("setCametion"," - camera null");
            return;
        }

        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(0, info);

        WindowManager winManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        int rotation = winManager.getDefaultDisplay().getRotation();

        int degrees = 0;

        switch (rotation)
        {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
        {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }
    public void captureImage(View v) throws IOException {
        //take the picture
        camera.takePicture(null, null, jpegCallback);
    }

    public void refreshCamera() {
        if (surfaceHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            camera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        try {
            camera.setPreviewDisplay(surfaceHolder);

            camera.startPreview();
          //  setCameraDisplayOrientation();
        } catch (Exception e) {

        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        refreshCamera();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            // open the camera
            camera = Camera.open();
        } catch (RuntimeException e) {
            // check for exceptions
            System.err.println(e);
            return;
        }
        Camera.Parameters param;
        param = camera.getParameters();
        param.setRotation(90); //set rotation to save the picture

        camera.setDisplayOrientation(90); //set the rotation for preview camera
        // modify parameter
        param.setPreviewSize(352, 288);
        //param.set("orientation", "landscape");
        //param.set("rotation", 90);
        camera.setParameters(param);
        try {
            // The Surface has been created, now tell the camera where to draw
            // the preview.
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
           // setCameraDisplayOrientation();

        } catch (Exception e) {
            // check for exceptions
            System.err.println(e);
            return;
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // stop preview and release camera
        camera.stopPreview();
        camera.release();
        camera = null;
    }
}
