package com.pkb.cameragallery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

public class PhotoActivity extends AppCompatActivity {

    int ACTIVITY_REQUEST_CODE=1;
    int ACTIVITY_RESULT_CODE=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
    }

    public void add(View v){
        Intent in=new Intent(getApplicationContext(),CameraGalleryImageActivity.class);
        startActivityForResult(in,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==ACTIVITY_REQUEST_CODE){
            if(resultCode==ACTIVITY_RESULT_CODE){
                Log.e("DATA RESPONCE",""+data.getExtras().getString("path"));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
